// Pour n'afficher la cr�ation de site qu'aux membres du groupe 
var createSiteVisible = userHasGroup(user.name, 'SiteCreators');
model.createSiteVisible = createSiteVisible;

function userHasGroup(username, group) {
   var result = remote.call("/api/people/" + stringUtils.urlEncode(username) + "?groups=true");
   if (result.status == 200 && result != "{}")
   {
      var user = eval('(' + result + ')');
     
      var groups = new Array();
      groups = user.groups;
      var mygroups = "";
      for (i=0; i<groups.length; i++)
      {                   
         if (groups[i].itemName == "GROUP_"+group || groups[i].itemName == "GROUP_ALFRESCO_ADMINISTRATORS"){
        return true; // found group
      }else{
        mygroup = mygroups+groups[i].displayName;
      }
      }
     
      return false;
   }
   else return false;
}
// Fin ajout


function main()
{
   // Check for IMAP server status
   var result = remote.call("/imap/servstatus"),
      imapServerEnabled = (result.status == 200 && result == "enabled");

   // Prepare the model for the template
   model.imapServerEnabled = imapServerEnabled;
   
   result = remote.call("/isreadonly");
   model.isReadOnly = (result.status == 200 && result == "true");
}

main();