(function (global) {
    //PLACE ALL STING DEPENDENCIES AT THE TOP OF THIS FILE SO CHANGES ARE LESS PAINFUL
    /* base rel path for loading all script */
    var _scriptsBasePath = "/connect/js/";
    //set to true if the browser is not IE 8 or below to load the most minimal
    //standards complient XDM and toolkits
    var isNotIE8 = (function (){
        var rv = 100;
        if (navigator.appName == 'Microsoft Internet Explorer'){
            var ua = navigator.userAgent;
            var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
            if (re.exec(ua) != null){
                rv = parseFloat( RegExp.$1 );
            }
        }
        return (rv >= 9) ? true : false;
    })();
    //filenames for various combos of connect scripts and css to be loaded based on specific app needs
    var tibIncludes = {
        pagebus: (isNotIE8) ? 'pagebus-ie8plus-min' : 'pagebus',
//        pagebus: 'pagebus-ie8plus', //excludes ie7 xdm, components- FIM, JSON2
//        pagebus: 'pagebus-ie8plus-min',
//        pagebus: 'pagebus', //full version, load conditionally for IE7 only
//        pagebus: 'pagebus-min',
        tibbrPagebus: 'tibbr.pagebus',
//        tibbrPagebus: 'tibbr.pagebus-min',
        tibMin: "tib-min",
//        pagebusPlus: 'pagebus_plus',
        plugins: "plugins",
//        plugins: "plugins-min",
//        tibPlugins: 'tib_plugins_min',
        pluginCSS: "/connect/stylesheets/tibbr_share-min.css",
//        connectProxy: '/connect/connect_proxy'
//        connectProxy: '/connect/connect_proxy_min'
        connectProxy: (isNotIE8) ? '/connect/connect_proxy_min' : '/connect/connect_proxy_ie'
    };
    //url path strings
    var tibURLs = {
        usersBySession: "/users/find_by_session",
        pluginsTranslate: "/plugins/connect_translate",
        connectConfig: "/plugins/connect_config",
        tibbrPath: "/tibbr", //default url path to tibbr server instance
        //tibbrPath: "",
        scriptsPath: _scriptsBasePath
    };
    var tibTopics = {
        proxyConnected: "tibbr:proxy:connected",
        loginRequest: "tibbr:login:request",
        loginResponse: "tibbr:login:response",
        loadTibbrPagebus: "load:" + tibIncludes.tibbrPagebus, //TIB.topics.loadTibbrPagebus
        loadPagebus: "load:" + tibIncludes.pagebus, //TIB.topics.loadPagebus
        loadPlugins: "load:" + tibIncludes.plugins
    };
    /* translations/ locales  */
    var i18n = {
        share: "Share",
        follow: "Follow",
        unfollow: "Unfollow",
        like: "Like",
        unlike: "Unlike",
        first_one: "Be the first one to like this",
        you: "You",
        and: " and ",
        like_this: " like this",
        likes_this: " likes this",
        one_other: " and 1 other like this",
        others: " others like this",
        not_logged_in: "You need to login to tibbr.",
        login: " Click <a href='#' class='tib-login-link'>here</a> to login."
    };
    /***
     * __tib__csAttr: get the current loading script with name,
     *  used extract attributes mentioned in current loading script
     *  @param{string} : name of attribute to extract from script tag or query string of script src
     *  @param{string} : name of file to look, like TIB.js
     *  @param{bool} : flag for read script tag attribute or query param, true for query param
     *
     *  @return {string} : value of matched attribute or undefined
     * */
    if (global["__tib__csAttr"] === void(0)) {
        global["__tib__csAttr"] = function (name, scriptName, queryString) {
            var allScripts = document.scripts ? document.scripts : document.getElementsByTagName("script");
            var scriptTag;
            var queryParams = {};
            //do we really need to loop through the entire scripts array???
            for (var i = allScripts.length; i >= 0; i--) {
                if (allScripts[i - 1] == null) { //checking for dom nodes
                    continue;
                }
                var src = allScripts[i - 1].getAttribute("src");
                if (src && src.indexOf(scriptName) != -1) {
                    scriptTag = allScripts[i - 1];
                    src.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (i, k, v) {
                        queryParams[k] = v;
                    });
                    return queryString ? queryParams[name] : scriptTag.getAttribute(name);
                }
            }
        };
    }
    /* name of callback funtion extracted from TIB.js script tag */
    var tibInitCallback = __tib__csAttr("callback", "TIB.js", true);
    /* object name default to TIB */
    var objName = __tib__csAttr("tib-obj-name", "TIB.js") || "TIB";
    var that = this;
    /* define TIB object */
    var TIB = global[objName] = function(){};
    TIB.topics = tibTopics;
    TIB.tibURLs = tibURLs;
    /* hold configuration fetched from server */
    TIB._serverConfig;
    TIB.translate = i18n;
    /* object to trace added pagebus client */
    var _pagebusClients = {};
    /* TIB version */
    TIB.version = "1.0.0";
    /* holds tibbr host */
    TIB._host = "";
    /* define currentUser  */
    TIB.currentUser = null;
    /* currentuser login status  */
    TIB.loggedIn = false;
    /****
     * logging variables and methods, not being use should be used across the framework
     *
     * */
    /*  mode, setting inside global scope. debug/undefined  */
    TIB.__mode = global["__tib__mode"];
    /* use console if debug mode is set */
    if (TIB.__mode === "debug" && global.console) {
        TIB.logger = global.console;
    } else {
        TIB.logger = {
            log: function () {
            },
            info: function () {
            }
        }
    }
    /**
     * all private variable declearations
     ***/
    /* track number of all required script to be loaded */
    var _scriptProcessing = 0;
    /***
     * Custom event mechanism to execute multiple callback on custom events
     *  object to store event callbacks */
    var eventQueue = {};
    /* initialize event object and attach to TIB */
    var _events = TIB.__events = {
        on: function (name, callback) {
            eventQueue[name] = eventQueue[name] || [];
            eventQueue[name].push(callback);
        },
        execute: function () {
            var name = arguments[0];
            var args = [].slice.call(arguments, 1);
            var cq = eventQueue[name] || [];
            for (var i in cq) {
                if (typeof(cq[i]) === "function") {
                    cq[i](args);
                }
            }
        },
        off: function (name) {
            eventQueue[name] = [];
        }
    };
    /* holds accessToken if given */
    var _accessToken;
    var _scripts = {
        /* include script to page and call given function when loaded
         * @param{string}: name of script file like TIB, jquery etc
         * @param{function}: callback to be called when script is loaded
         *    */
        include: function (name, callback) {
            var s = global.document.createElement("script");
            s.setAttribute("type", "text/javascript");
            s.setAttribute("tib-obj-name", objName);
            s.setAttribute("src", _protocal() + TIB._host + _scriptsBasePath + name + ".js");
            /*  loading script count +1  */
            _scriptProcessing += 1;
            /* register callback against load:{scriptName} event*/
            TIB.__events.on("load:" + name, function () {
                /*  loading script count - 1 as script is loaded on page */
                _scriptProcessing -= 1;
                /* execute the callback */
                if (typeof(callback) === "function") {
                    callback();
                }
                if (_scriptProcessing === 0) {
                    _scriptsOnLoad();
                }
            });
            document.getElementsByTagName('head')[0].appendChild(s);
        }
    };
    /* ssl flag  */
    var _sslEnable = false;
    /* return protocol to be use  */
    var _protocal = function () {
        return (_sslEnable ? "https:" : "") + "//";
    };
    /* function to run when all scripts are loaded  */
    var _scriptsOnLoad = function () {
        if (typeof(_onScripsLoadsCallback) === "function"){
            _onScripsLoadsCallback();
        }
    };
    /* variable to hold callback to be executed when all scripts file loaded */
    var _onScripsLoadsCallback = null;
    var _registerStylesheet = function (url) {
        var s = global.document.createElement("link");
        s.setAttribute("type", "text/css");
        s.setAttribute("rel", "stylesheet");
        s.setAttribute("media", "all");
        s.setAttribute("href", _protocal() + TIB._host + url);
        document.getElementsByTagName('head')[0].appendChild(s);
    };
    /* Function called when cross domain channel is established/connected*/
    ///hard dependency on pagebus
    var _initializedChannel = function (callback) {
        /* when tibbr:proxy connected do init stuff*/
        TIB.PageBus.subscribe( tibTopics.proxyConnected, function () {
            /* load translation files */
            if (TIB._pluginsEnabled === true) {
                _api({
                    url: tibURLs.connectConfig,
                    method: "GET",
                    onResponse: function (data) {
                        if (!data.errors) {
                            TIB.translate = data.locales;
                            TIB._serverConfig = data.config;
                            /* use default client id when client ID is not given while init*/
                            if(!TIB.client_id)
                            {TIB.client_id = TIB._serverConfig.default_client_id;}
                        }
                    }
                })
            }
            /* initialize the current user by finding from session
             * TODO: This need to fix for find by access_token
             *
             * */
            _api({
                url: tibURLs.usersBySession,
                method: "GET",
                onResponse: function (data) {
                    if (data && data.id) {
                        TIB.currentUser = data;
                        TIB.loggedIn = true;
                        _events.execute("login", data);
                    }
                    if (typeof callback == "function"){
                        callback();
                    }
                }
            })
            /* Unsubscribe the connected event */
            TIB.PageBus.unsubscribe( tibTopics.proxyConnected);
        });
    };
    /***
     * Create pagebus client iframe.
     * @param{string}: url of iframe src
     * @param{string}: identifier for client, will be used as iframe id too
     * @param{domElement}: container where iframe need to loaded
     * @param{object}:set of attributes assign to created iframe
     * @param{function}: callback to be called when the frame client connected
     * */
    var _createProxyClient = function (url, id, container, frameOptions, onConnect) {
        container = container || document.createElement("div");
        frameOptions = frameOptions || {
            scrolling: "no",
            style: {
                border: "white solid 1px",
                width: "0",
                height: "0",
                visibility: "hidden",
                display: "none"
            }
        };
        /* remove the exiting iframe client matiching with id, avoid duplicate cliet pagebus error */
        if (_pagebusClients[id]){
            _removeProxyClient(id);
        }
        var client = new TIB.OpenAjax.hub.IframeContainer(global.tibbrConnectManagedHub, id,
            {
                Container: {
                    onSecurityAlert: function (source, alertType) {
                    },
                    onConnect: function () {
                        onConnect.call()
                    }
                },
                IframeContainer: {
                    parent: container,
                    iframeAttrs: frameOptions,
                    uri: url,
                    tunnelURI: TIB._tunnelUrl
                }
            });
        /* keep reference of client */
        _pagebusClients[id] = client;
        return client._iframe;
    };
    /***
     * remove pagebus client iframe.
     * @param{string}: identifier for client, will be used as iframe id too
     * @param{function}: callback to be called when the frame client removed
     * */
    var _removeProxyClient = function (id, callback) {
        try {
            TIB.PageBus.disconnectContainer(_pagebusClients[id]);
            if (typeof(callback) === "function") {
                callback();
            }
        }
        catch (e) {}
    };
    /***
     * setup the cross domain communication channel for javascript api, TIB.api
     * @param{function}: callback to be called after setup
     * */
    var _setupCORS = function (callback) {
        TIB.OpenAjax = TIB.__pageBus.openAjax;
        TIB.smash = TIB.__pageBus.smash;
        TIB.PageBus.init();

        var cont = document.createElement("div");
        var cb = function () {
            _initializedChannel(callback);
        };
        cont.id = "tib-connet-proxy";
        var hostprefix = encodeURIComponent(TIB._host);
        var acc_token = _accessToken || "";
        var proxyConnect = _createProxyClient(_protocal() + TIB._host + tibIncludes.connectProxy + ".html?obref=" +
            objName + "&acctkn=" + acc_token + "&hpfx=" + hostprefix, "proxyConnect", document.getElementsByTagName("body")[0], false, cb);
    };
    /***
     * setup TIB.api
     * @param{function}: callback to be called after setup
     * */
    var _setup = function (callback) {
        _onScripsLoadsCallback = function () {
            _setupCORS(callback);
        };
        // possible scenarios
        //non-tibbr managed window
        //non-tibbr managed window w/ plugins
        //tibbr managed window - non tibbr iframe
        //window.postMessage support vs ie7
        if (TIB._pluginsEnabled) {
            _registerStylesheet(tibIncludes.pluginCSS);
        }
        /* load pagebus.js*/
        //remove dep on PB
        _scripts.include( tibIncludes.pagebus);//move functionality over to tibbr.pagebus
        /* load tibbr.pagebus.js*/
        _scripts.include(tibIncludes.tibbrPagebus);
        //_scripts.include(tibIncludes.pagebusPlus);
        /*load plugins js and css when plugins enabled*/
        if (TIB._pluginsEnabled) {
            _registerStylesheet(tibIncludes.pluginCSS);
            _scripts.include(tibIncludes.tibMin, function () {
                _scripts.include(tibIncludes.plugins);
            });
//            _scripts.include(tibIncludes.tibPlugins);
        }
        /*load parent_connector lib used for settingup height when app is loaded in tibbr iframe*/
        if (TIB._parentTibbr) {
            _scripts.include("parent_connector");
        }
    };
    /***
     * manages the response of api call coming from pagebus and call the onResponse handler
     * @param{object}: data response from pagebus client
     * @param{object}: api params persisted from TIB.api call
     *
     **/
    var __apiResponseHandler = function (data, params) {
        /* call the api onResponse*/
        params.onResponse(data.r, data.rc);
        /* unsubscribe the api pagebus event*/
        TIB.PageBus.unsubscribe(params.handlerId);
    };
    /***
     * exposed as TIB.api to call tibbr apis
     * @param{object}: data response from pagebus client
     ** ** @@url{string} : url of api to be called
     ** ** @@method{string}: http method for api[get,post,delete,put]
     ** ** @@type{string}: format, xml/json
     *  ** @@params{object}: api parameters
     *  ** @@onResponse{function, response}: callbak will called when api response come in from pagebase
     ** **
     **/
    var _api = function (args) {
        args["handlerId"] = "api:response:" + Math.floor(Math.random() * 1111);
        args.url = _protocal() + TIB._host + "" + args.url;
        /* assign client id in each api call if not provided by end user */
        if(args.params && !args.params.client_id && TIB.client_id){
            args.params.client_id = TIB.client_id;
        }

        var params = {
            u: args.url,
            m: args.method,
            t: args.type,
            d: args.params,
            hid: args.handlerId
        };
        TIB.PageBus.publish("api:call", params);
        TIB.PageBus.subscribe(args["handlerId"], function (data) {
            __apiResponseHandler(data, args);
        });
    };
    /***
     * exposed as TIB.login
     * @param{function}:
     *
     * */
    var _login = function (callback) {
        TIB.PageBus.publish( tibTopics.loginRequest);
        if (callback) {
            TIB.onLogin(callback);
        }
        TIB.PageBus.subscribe( tibTopics.loginResponse, function (data) {
            _api({
                //url: "/users/find_by_session",
                url: tibURLs.usersBySession,
                method: "GET",
                onResponse: function (data) {
                    if (data && data.id) {
                        TIB.currentUser = data;
                        TIB.loggedIn = true;
                        _events.execute("login", data);
                    }
                    if (typeof callback === "function") {
                        callback();
                    }
                }
            })
            TIB.PageBus.unsubscribe( tibTopics.loginResponse);
        });
    };
    /*
     *assign _login to TIB.login
     **/
    TIB.login = _login;
    /***
     * add function callback to be execute on init
     * @param{function}:  callback to be called after init
     *
     * */
    TIB.onInit = function (callback) {
        _events.on("initialize", callback);
    };
    //dgs - general purpose way to initialize a plugin i.e share, like, comment etc
    TIB.initPlugin = function(type, plugin){
        _events.on("initialize", function(){
            TIB.Plugins[type](plugin);
        });
    };
    /***
     * add function callback to be execute on login
     * @param{function}:  callback to be called after init
     *
     * */
    TIB.onLogin = function (callback) {
        _events.on("login", callback);
    };
    /* Expose login and initialized function to TIB object */
    TIB.initialized = false;
    /* Expose iframe client apis to TIB
     * must be used by other supporting lib to create iframe client
     *   */
    TIB.__addProxyClient = _createProxyClient;
    TIB.__removeProxyClient = _removeProxyClient;

    /*
     * Initialize the TIB object
     * @params{object}: configurations
     * ** @@tunnelUrl{string}: Url of tunnel.html file
     * ** @@access_token{string}: access_token token for current_user if given
     * ** @@prefix{string}: url prefix, i.e /tibbr
     * ** @@renderInTibbr{bool}: if true parent utility will be loaded
     * ** @@plugins{bool}: if true plugin utility will be loaded
     * ** @@host{string}: tibbr host
     * ** @@ssl{bool}: use https protocol
     * ** @@onInitialize{function}: callback to be called after initialized
     **/
    TIB.init = function (options) {
        if (TIB.initialized) {
            return false;
        }
        _accessToken = options.access_token;
        //var host = options.host.replace(/^https?:\/\//i, "");
        var host = options.host.replace(/^https?:\/\//i, "").replace(/\/.*/, "");
        if (options.prefix === void(0) && host.match(/localhost:/i)) {
            options.prefix = "";
        }
        //todo: we need to allow the user to pass in a single host string and parse it for host, port, path
        var pathPrefix = typeof(options.prefix) !== "undefined" ? options.prefix : TIB.tibURLs.tibbrPath;
        global.__tibHost = TIB._host = host + pathPrefix;
        TIB._tunnelUrl = options.tunnelUrl + "?objname=" + objName;
        if (options.ssl) {
            _sslEnable = true;
        }
        TIB._protocal = _protocal;
        TIB.client_id = options.client_id;
        TIB._parentTibbr = options.renderInTibbr;
        if(options.plugins){
            TIB._pluginsEnabled = true;
        }
        //dgs - allows developer to initalize plugins async by just declaring a plugin property object literal
        //now supports multiple of the same plugin type by including their config objects in an array
        var initPlugins = function(plugins){
            for(var plugin in plugins){
                if(plugins[plugin] instanceof Array){
                    var type = plugin;
                    for(var z = 0, y = plugins[plugin].length; z < y; z++){
                        TIB.initPlugin( type, plugins[plugin][z]);
                    }
                }else if(typeof plugins[plugin] === 'object'){
                    TIB.initPlugin(plugin, plugins[plugin]);
                }else{
                    console.warn('failed to initialize ' + plugin + 'plugin');
                }
            }
        };
        if(typeof options.plugins === 'object'){
            initPlugins(options.plugins);
        }

        TIB.api = _api;
        _setup(function () {
            if (TIB.initialized) {
                return false;
            }
            TIB.initialized = true;
            TIB.api = _api;
            _events.execute("initialize");
            if (tibInitCallback && typeof(global[tibInitCallback]) === "function") {
                global[tibInitCallback](TIB);
            }
            if (typeof(options.onInitialize) === "function") {
                options.onInitialize();
            }
        });
    };
    if (global.tib_init) {
        TIB.init(global.tib_init);
    }
})(window);
