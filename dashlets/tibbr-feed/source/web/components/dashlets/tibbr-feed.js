if (typeof ISS == "undefined" || !ISS)
{
   var ISS = {};
   ISS.dashlet = {};
}

/**
 * ISS.dashlet.tibbrFeed
 */
(function()
{
   /**
    * YUI Library aliases
    */
   var Dom = YAHOO.util.Dom,
      Event = YAHOO.util.Event;

   /**
    * tibbrFeed constructor.
    * 
    * @param {String} htmlId The HTML id of the parent element
    * @return {ISS.dashlet.tibbrFeed} The new tibbrFeed instance
    * @constructor
    */
   ISS.dashlet.tibbrFeed = function tibbrFeed_constructor(htmlId)
   {
            ISS.dashlet.tibbrFeed.superclass.constructor.call(this, "ISS.dashlet.tibbrFeed", htmlId);

        /**
         * Register this component
         */
        Alfresco.util.ComponentManager.register(this);

        /**
         * Load YUI Components
         */
        Alfresco.util.YUILoaderHelper.require(["button", "container", "datasource", "datatable", "paginator", "json", "history", "tabview"], this.onComponentsLoaded, this);

        return this;
    };

    YAHOO.extend(ISS.dashlet.tibbrFeed, Alfresco.component.Base,
    {
                /**
         * Object container for initialization options
         *
         * @property options
         * @type object
         */
        options:
        {
            componentId: "",
            siteId: "",
            title: "tibbr subject feed",
            tibbrsubj: "Tibco",
            tibbrhost: "https://tibco.tibbr.com",
        },

        widgets: {},

        /**
         *      Fired by YUI when parent element is available for scripting
         *
         *      @method onReady
         */

        onReady: function tibbrFeed_onReady()
        {
            var me = this;
        },

        /**
         * Called when the user clicks the config tibbrFeed link.
         * Will open a tibbrFeed config dialog
         *
         * @method onConfigtibbrFeedClick
         * @param e The click event
         */
        onConfigtibbrFeedClick: function tibbrFeed_onConfigtibbrFeedClick(e)
        {

            var actionUrl = Alfresco.constants.URL_SERVICECONTEXT + "modules/tibbr-feed/config/" + encodeURIComponent(this.options.componentId);

            if (!this.configDialog)
            {
                this.configDialog = new Alfresco.module.SimpleDialog(this.id + "-configDialog").setOptions(
                {
                    width: "30em",
                    templateUrl: Alfresco.constants.URL_SERVICECONTEXT + "modules/tibbr-feed/config",
                    actionUrl: actionUrl,
                    onSuccess:
                    {
                        fn: function tibbrFeed_onConfigtibbrFeed_callback(response)
                        {
                            var obj = response.json;

                            // Save values for new config dialog openings
                            this.options.title = (obj && obj.title) ? obj.title : this.options.title;
                            this.options.tibbrsubj    = (obj && obj.tibbrsubj) ? obj.tibbrsubj : this.options.tibbrsubj;
                            this.options.tibbrhost = (obj && obj.tibbrhost) ? obj.tibbrhost : this.options.tibbrhost;

                            // Update dashlet body with new values
                            Dom.get(this.configDialog.id + "-title").value = obj ? obj.title : "";
                            Dom.get(this.configDialog.id + "-tibbrsubj").value    = obj ? obj.tibbrsubj : "";
                            Dom.get(this.configDialog.id + "-tibbrhost").value = obj ? obj.tibbrhost : "";
                        },
                        scope: this
                    },

                    doSetupFormsValidation:
                    {
                        fn: function tibbrFeed_doSetupForm_callback(form)
                        {
                            form.addValidation(this.configDialog.id + "-title", Alfresco.forms.validation.mandatory, null, "keyup");
                            form.setShowSubmitStateDynamically(true, false);

                            Dom.get(this.configDialog.id + "-title").value = this.options.title;
                            Dom.get(this.configDialog.id + "-tibbrsubj").value = this.options.tibbrsubj;
                            Dom.get(this.configDialog.id + "-tibbrhost").value = this.options.tibbrhost;
                        },
                        scope: this
                    }
                });
            }

            this.configDialog.setOptions(
            {
                actionUrl: actionUrl
            }).show();
        }

    });
})();
