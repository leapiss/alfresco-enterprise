function main()
{
    var hasConfigPermission = false;
        // Call the repository to see if the user is site manager or not

    if (page.url.templateArgs.site != null) // Site or user dashboard?
    {
        json = remote.call("/api/sites/" + page.url.templateArgs.site + "/memberships/" + encodeURIComponent(user.name));

        if (json.status == 200)
        {
            var obj = eval('(' + json + ')');
            if (obj)
            {
                hasConfigPermission = (obj.role == "SiteManager");
            }
        }
    }
    else
    {
      hasConfigPermission = true; // User Dashboard  
    }
    
    model.hasConfigPermission = hasConfigPermission;

    var title = args.title;
    var tibbrsubj = args.tibbrsubj;
    var tibbrhost = args.tibbrhost;

    // Create XML object to pull values from
    // configuration file
    var conf = new XML(config.script);

    // Use the defaults from the XML configuration file
    // (tibbr.get.config.xml) if no values in args array
    if (!title)
    {
    title = conf.title[0].toString();
    }
    if (!tibbrsubj)
    {
    tibbrsubj = conf.tibbrsubj[0].toString();
    }

    if (!tibbrhost) 
    {
    tibbrhost = conf.tibbrhost[0].toString();
    }

    // Set values on the model for use in templates
    model.title = title;
    model.tibbrsubj = tibbrsubj;
    model.tibbrhost = tibbrhost;
}

main();
