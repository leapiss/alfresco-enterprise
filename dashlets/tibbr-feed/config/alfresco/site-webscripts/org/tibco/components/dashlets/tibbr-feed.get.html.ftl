<#assign el=args.htmlid?js_string>

<script type="text/javascript">//<![CDATA[

function getContextURLPath()
{
var rootUrl = location.protocol;
rootUrl = rootUrl+"//"+location.host
var path = location.pathname;
var tempStr = path.split('/');
rootUrl = rootUrl+"/"+tempStr[1];
//alert("Final Root URL=>"+rootUrl);

return rootUrl;
}

var contextUrl = getContextURLPath();

(function() {

  var tibbrFeed = new ISS.dashlet.tibbrFeed("${el}").setOptions(
  {
    "componentId": "${instance.object.id}",
    "title": "${title}",
    "tibbrsubj": "${tibbrsubj}",
    "tibbrhost": "${tibbrhost}",
  }).setMessages(${messages});
    
  new Alfresco.widget.DashletResizer("${el}", "${instance.object.id}");

   /**
    * Create a new custom YUI event and subscribe it to the tibbrFeed onConfigtibbrFeedClick
    * function. This custom event is then passed into the DashletTitleBarActions widget as
    * an eventOnClick action so that it can be fired when the user clicks on the Edit icon
   */
   var editDashletEvent = new YAHOO.util.CustomEvent("onDashletConfigure");
   editDashletEvent.subscribe(tibbrFeed.onConfigtibbrFeedClick, tibbrFeed, true);
   new Alfresco.widget.DashletTitleBarActions("${args.htmlid?html}").setOptions(
   {
      actions:
      [

<#if hasConfigPermission>
         {
            cssClass: "edit",
            eventOnClick: editDashletEvent,
            tooltip: "${msg("dashlet.edit.tooltip")?js_string}"
         },
</#if>
         {
            cssClass: "help",
            bubbleOnClick:
            {
               message: "${msg("dashlet.help")?js_string}"
            },
            tooltip: "${msg("dashlet.help.tooltip")?js_string}"
         }
      ]
   });
})();

var tibbrsubj= "${tibbrsubj?js_string}";
var tibbrhost= "${tibbrhost?js_string}";
var tibbrtunnel= contextUrl + "/res/extras/components/dashlets/tunnel.html";
TIB.init({
    host: tibbrhost, //Provide main tibbr application host
    tunnelUrl: tibbrtunnel, //Place tunnel.html on your server and provide correct URL to it.
    plugins: true
});

TIB.onInit(function(){
    TIB.Plugins.comments({container:"tibbr_comments", subject_id: tibbrsubj, file: false, link: true, profile_hover: false});
});
//]]></script>
<div class="dashlet tibbr">
  <div id="${args.htmlid}-title" class="title">
    ${title}
  </div>
  <div class="body scrollablePanel" id="${args.htmlid}-iframeWrapper">
    <div id="tibbr_comments" frameborder="0" scrolling="auto" width="100%" height="100%"></div>
    <div class="resize-mask"></div>
  </div>
</div>
