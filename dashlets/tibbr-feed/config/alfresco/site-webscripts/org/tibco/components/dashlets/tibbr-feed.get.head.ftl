<#include "/org/alfresco/components/component.head.inc">
<!-- tibbr JS -->
<@script type="text/javascript" src="${url.context}/res/extras/components/dashlets/TIB.js"></@script>
<!-- Simple Dialog -->
<@script type="text/javascript" src="${page.url.context}/modules/simple-dialog.js"></@script>
<!-- Resize -->
<@script type="text/javascript" src="${page.url.context}/res/yui/resize/resize.js"></@script>
<!-- tibbr dashlet JS -->
<@script type="text/javascript" src="${page.url.context}/res/components/dashlets/tibbr-feed.js" />
