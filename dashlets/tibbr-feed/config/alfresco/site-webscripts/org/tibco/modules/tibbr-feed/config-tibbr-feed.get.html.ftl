<#assign el=args.htmlid?html>

<div id="${el}-configDialog" class="config-tibbrfeed">
   <div class="hd">${msg("label.dialogTitle")}</div>

   <div class="bd">
      <form id="${el}-form" action="" method="POST">
         <div class="yui-gd">
            <div class="yui-u first"><label for="${el}-title">${msg("label.title")}:</label></div>
            <div class="yui-u"><input  id="${el}-title" type="text" name="title" value="" maxlength="30" />&nbsp;*</div>
         </div>
         <div class="yui-gd">         
            <div class="yui-u first"><label for="${el}-tibbrsubj">${msg("label.tibbrsubj")}:</label></div>
            <div class="yui-u"><input  id="${el}-tibbrsubj" type="text" name="tibbrsubj" value="" maxlength="30" />&nbsp;*</div>
         </div>
         <div class="yui-gd">
            <div class="yui-u first"><label for="${el}-tibbrhost">${msg("label.tibbrhost")}:</label></div>
            <div class="yui-u"><input  id="${el}-tibbrhost" type="text" name="tibbrhost" value="" maxlength="75" />&nbsp;*</div>
         </div>
         <div class="bdft">
            <input type="submit" id="${el}-ok" value="${msg("button.ok")}" />
            <input type="button" id="${el}-cancel" value="${msg("button.cancel")}" />
         </div>
      </form>
   </div>

</div>
