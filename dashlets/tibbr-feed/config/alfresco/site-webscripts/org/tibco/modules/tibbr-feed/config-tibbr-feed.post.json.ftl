<#escape x as jsonUtils.encodeJSONString(x)> {
   "title": "${title!''}",
   "tibbrsubj": "${tibbrsubj!''}",
   "tibbrhost": "${tibbrhost!''}"
}
</#escape>