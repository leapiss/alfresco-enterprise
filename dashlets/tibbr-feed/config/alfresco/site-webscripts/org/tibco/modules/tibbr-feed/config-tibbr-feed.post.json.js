/*
*      tibbr feed configuration component POST method
*/

function main()
{

        var c = sitedata.getComponent(url.templateArgs.componentId);

        var saveValue = function(name, value)
        {
        c.properties[name] = value;
        model[name] = value;
        }

        saveValue("title", String(json.get("title")));
        saveValue("tibbrsubj", String(json.get("tibbrsubj")));
        saveValue("tibbrhost", String(json.get("tibbrhost")));

        c.save();

}

main()