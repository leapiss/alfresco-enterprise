<#macro dateFormat date>${date?string("yyyy-dd-MM'T'HH:mmZ")}</#macro>
<#escape x as jsonUtils.encodeJSONString(x)>
{
"documents": 
    [
        <#list docs as child>
        {
            "site": "${site}",
            "nodeRef": "${child.nodeRef}",
            "id": "${child.id}",
            "name": "${child.properties["name"]}",
            "title": "${child.properties["title"]!}",
            "creator": "${child.properties["creator"]}",
            "description": "${child.properties["description"]!}",
            "categories": [
                    <#if child.properties["cm:categories"]?exists>
                    <#list child.properties["cm:categories"] as category>
                    {
                    "name": "${category.name!}"
                    }<#if category_has_next>,</#if>
                    </#list></#if>
                    ],
            "created": "<@dateFormat child.properties["created"] />",
            "modified": "<@dateFormat child.properties["modified"] />"
        }<#if child_has_next>,</#if>
        </#list>
    ]
    
}
</#escape>
