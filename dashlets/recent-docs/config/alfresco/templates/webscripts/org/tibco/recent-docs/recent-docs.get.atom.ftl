<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <generator version="${server.version}">Alfresco (${server.edition})</generator>
  <link rel="self" href="${absurl(url.full)?xml}" />
  <id>${absurl(url.full)?xml}</id>
  <title>Alfresco Recently Modified Documents: ${site} <#if filter?exists>- ${filter}</#if></title>
  <updated>${xmldate(date)}</updated>
<#list docs as child>
  <entry xmlns='http://www.w3.org/2005/Atom'>
    <title><#if child.properties["title"]?exists><![CDATA[${child.properties["title"]?html}]]><#else><![CDATA[${child.properties["name"]?html}]]></#if></title>
    <link rel="alternate" type="text/html" href="https://alfresco.tibco.com/share/page/document-details?nodeRef=${child.nodeRef}" />
    <id>urn:uuid:${child.id}</id>
    <updated>${xmldate(child.properties["modified"])}</updated>
        <summary type="html"><![CDATA[The Document <#if child.properties["title"]?exists>${child.properties["title"]?html}<#else>${child.properties["name"]}</#if>
	 <#if child.properties["modified"]?datetime?string.short != child.properties["created"]?datetime?string.short >has been updated by ${child.properties["modifier"]} on ${child.properties["modified"]?datetime?string.long}.<br />
     <#else >has been uploaded by ${child.properties["creator"]}  on ${child.properties["modified"]?datetime?string.long}.<br /></#if>
	 <#if child.properties["description"]?exists>${child.properties["description"]!""}<br /></#if>
     <#if child.properties["cm:categories"]?exists>With categories: <#list child.properties["cm:categories"] as category>${category.name}<#if category_has_next>, </#if></#list><br /></#if>]]>
     <#if child.properties["cm:taggable"]?exists>With tags: <#list child.properties["cm:taggable"] as tag>${tag.name}<#if tag_has_next>, </#if></#list><br /></#if>]]>
	 </summary>
    <author> 
      <name>${child.properties["creator"]?xml}</name>
    </author> 
  </entry>
</#list>
</feed>
