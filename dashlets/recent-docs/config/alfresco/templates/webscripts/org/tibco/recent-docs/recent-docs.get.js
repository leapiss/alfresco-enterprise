const SITES_SPACE_QNAME_PATH = "/app:company_home/st:sites/";
var filter = null;

for (var arg in args)
{
  if (arg == "site")
  {
    model.site = args[arg];
  }
  else if (arg == "filter")
  {
  // Remove extra slashes then convert to repository path
    model.filter = args[arg];
    filter = model.filter.replace(/^\/|\/$/g, "");
    filter = filter.replace(/ /g,"_x0020_");
    filter = filter.split("/").join("/cm\\:");
  }
}


if (filter != null)
{
  var strSearchPath = SITES_SPACE_QNAME_PATH + 'cm\:' + model.site + '/cm\:documentLibrary/cm\:' + filter + '//.\"'; 
}
else
{ 
  
  var strSearchPath = SITES_SPACE_QNAME_PATH + 'cm\:' + model.site + '/cm:documentLibrary/*//.\"';	
}	


var sort1 = 
{ 
  column: "@{http://www.alfresco.org/model/content/1.0}modified", 
  ascending: false 
}; 

var sort2 = 
{ 
  column: "@{http://www.alfresco.org/model/content/1.0}created", 
  ascending: false
}; 

var paging = 
{ 
  maxItems: 50, 
  skipCount: 0 
}; 

var alfQuery = 
     'PATH\:"' + strSearchPath +
       ' AND -TYPE\:"cm:thumbnail"' +
         ' AND -TYPE\:"cm:failedThumbnail"' +
           ' AND -TYPE\:"cm:rating"' +
             ' AND -TYPE\:"cm:folder"' +
               ' AND -TYPE\:"rule:rule"' +
                 ' AND -TYPE\:"act:compositeaction"' +
                   ' AND -TYPE\:"act:action"' +
                     ' AND -TYPE\:"act:actioncondition"' +
                       ' AND -TYPE\:"act:actionparameter"' +
                         ' AND -TYPE\:"fm:post"' +
               ' AND NOT ASPECT\:"sys:hidden"';
var def = 
{ 
  query: alfQuery, 
  store: "workspace://SpacesStore", 
  language: "fts-alfresco", 
  sort: [sort1, sort2], 
  page: paging 
}; 

results = search.query(def);

model.docs = results;
