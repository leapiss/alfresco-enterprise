<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <generator version="${server.version}">Alfresco (${server.edition})</generator>
  <link rel="self" href="${absurl(url.full)?xml}" />
  <id>${absurl(url.full)?xml}</id>
  <title>Alfresco Recently Modified Documents: ${site}</title>
  <updated>${xmldate(date)}</updated>
<#list docs as child>
  <entry xmlns='http://www.w3.org/2005/Atom'>
    <title><![CDATA[${child.name?html}]]></title>
    <link rel="alternate" type="text/html" href="${absurl(url.context)}/page/document-details?nodeRef=${child.nodeRef}" />
    <id>urn:uuid:${child.id}</id>
    <updated>${child.modified?datetime}</updated>
    <summary type="html"><![CDATA[
		 ${msg("feed.uploaded", child.name, child.creator)}<br />
	 <#if child.modifier?exists>${msg("feed.modified", child.modified, child.modifier)}<br /></#if>
	 <#if child.description?exists>${child.description!""}<br /></#if>
     <#if child.categories[0]?exists>${msg("feed.categories")} <#list child.categories as category> ${category.name}<#if category_has_next>, </#if></#list><br /></#if>]]>
	 </summary>
    <author> 
      <name>${child.creator?xml}</name>
    </author> 
  </entry>
</#list>
</feed>
