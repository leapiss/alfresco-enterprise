<div class="body scrollablePanel">
  <div id="getlatestdoc_item">
      <div id="getlatestdoc_item_afb">
      <#list docs as c>
<#assign fileExtIndex = c.name?last_index_of(".")>
<#assign fileExt = (fileExtIndex > -1)?string(c.name?substring(fileExtIndex + 1)?lower_case, "generic")>
      <a href="${url.context}/page/document-details?nodeRef=${c.nodeRef}">
        <img src="${url.context}/components/images/filetypes/${fileExt}-file-48.png" title="${c.name}" class="node-thumbnail" width="48" />
      </a>
    </div>
    <div id="getlatestdoc_item_info">
      <a href="${url.context}/page/document-details?nodeRef=${c.nodeRef}">${c.name}</a><br />
      ${c.name!''} <br />
      ${c.created} <br />
      ${c.description!''} <br />
    </#list>  
    </div>  
  </div>
</div>
