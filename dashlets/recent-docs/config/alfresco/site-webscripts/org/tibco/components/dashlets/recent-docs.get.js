function main()
{
    for (var arg in args)
    {
      if (arg == "site")
      {
        model.site = args[arg];
      }
    }
        // call the repository to get recent documents
        var connector = remote.connect("alfresco"); 
        var json = connector.call("/recent-docs?site=" + escape(model.site));
        if (json.status == 200)
        {
                obj = eval("(" + json + ")");
                model.docs = obj["documents"];
        }
        else
        {
                obj = eval("(" + json + ")");
                obj.name = "Error";
                model.docs = obj;
        }

}

main()
