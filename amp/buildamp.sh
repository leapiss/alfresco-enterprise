#!/bin/bash

#Set some basic variables
AMPDIR=$HOME/amp/
ALFDIR=$(pwd)/alfresco
SHADIR=$(pwd)/share


printf "Building Alfresco Amps\n"
# Build Alfresco amps and put them in their special place

#Find all amp folders, exclude root directory so it's not processed
find "$ALFDIR" "$SHADIR" -maxdepth 1 -type d \( ! -path "$ALFDIR" ! -path "$SHADIR" \) | while read PROCDIR; do
    #Helper variable to extract customization type from file path
    amptype="${PROCDIR%\/*}"
    #Report which customizations is being packaged
    echo -en "Processing ${PROCDIR//*\//}: "
    # Create destination if it's not there
    if [ ! -d "${AMPDIR}/${amptype//*\//}" ]; then
        mkdir -p "${AMPDIR}/${amptype//*\//}"
    fi
    # Zip directory contents placing the zip file in the amp directory:
    cd "${PROCDIR}" && zip -qr0 "${AMPDIR}/${amptype//*\//}/${PROCDIR//*\//}.amp" .
    #Report failure if it occurs
    case $? in 0) echo OK;; *) echo FAILED;; esac
done

